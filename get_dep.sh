set -x
echo ${PWD}
BMDP=${PWD}/lib/BMDP-synthesis.zip
DPATH=${PWD}/lib
cd ${DPATH}

sudo apt-get install libboost-all-dev
sudo apt-get install libmatio-dev
sudo apt-get install libhdf5-dev
sudo apt-get install libginac-dev
sudo apt-get install libblas-dev
sudo apt-get install liblapack-dev
sudo apt-get install libnlopt-dev

if [ ! -d "cubature" ]; then
  echo "--------------------------------"
  echo "working on cubature ..."
  git clone https://github.com/stevengj/cubature
  cd cubature
  cc -c hcubature.c
  cc -c pcubature.c
  ar rcs libhcubature.a hcubature.o
  ar rcs libpcubature.a pcubature.o
  rm hcubature.o
  rm pcubature.o
  mkdir lib
  mv libhcubature.a lib/
  mv libpcubature.a lib/
  echo "--------------------------------"
  cd ../
fi

if [ ! -d "BMDP-synthesis" ]; then
  echo "--------------------------------"
  echo "working on BMDP synthesis ..."
  cp ${BMDP} .
  unzip BMDP-synthesis.zip
  cd BMDP-synthesis
  make lib
  echo "--------------------------------"
  cd ../
fi

#if [ ! -d "armadillo" ]; then
  echo "--------------------------------"
  echo "working on armadillo ..."
  mkdir -p armadillo
  cd armadillo
  wget https://sourceforge.net/projects/arma/files/armadillo-9.200.5.tar.xz
  tar -xvf armadillo-9.200.5.tar.xz --strip-components 1
  rm armadillo-9.200.5.tar.xz
  ./configure
  make
  sudo make install
  echo "--------------------------------"
  cd ../
#fi
